" Good reference: http://dougblack.io/words/a-good-vimrc.html

execute pathogen#infect()

" NATIVE VIM CONFIGS
filetype indent on
syntax enable " syntax highlight
set number " show line numbers
set background=dark " dark color scheme
"colorscheme noctu
"set fillchars+=vert:\ " don't know what this does
" set linebreak 

" tab rule aligned w/ crockford js code style guide
" see: http://vim.wikia.com/wiki/Converting_tabs_to_spaces
" filetype plugin indent on
set tabstop=4 " number of spaces in tab when editing tabstop=4 " number of visual spaces per TAB
set shiftwidth=4 " unsure what difference is between tab sets
"set expandtab " expands tab to white spaces

"set softtabstop=4 " number of spaces in tab when editing
set showcmd " show command in bottom bar
"set cursorline " underline current line
set wildmenu " visual 'intellisense' autocomplete for command menu
set incsearch " search as characters are entered
"set hlsearch " highlight search matches
inoremap jj <ESC> " maps jj to <ESC> 
nmap ff :NERDTreeToggle<CR>
set t_Co=16 " in crosh same as 256
set statusline+=%F " show current filename is status bar
" is this working?
set omnifunc=javascriptcomplete#CompleteJS " enable JavaScript autocomplete

set backspace=indent,eol,start " fixing backspace not working on some envs

" NERDTree
autocmd vimenter * NERDTree " starts NERDTree plugin automatically
let g:NERDTreeDirArrows=0 " removes odd chars and adds path arrows
let NERDTreeShowHidden=1 " see hidden files in NERDTree

" --------------------------------------------
" Syntastic
" --------------------------------------------

" native vim configs recommended by sytastic
" see: https://github.com/scrooloose/syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" synatstic specific configs
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
"let g:syntastic_javascript_checkers = ['jshint', 'jscs']
let g:syntastic_javascript_checkers = ['jshint']

" -----------------------------------------------
" js-context-coloring
" color code by js scope (rather than syntax)
" see: https://github.com/bigfish/vim-js-context-coloring
" can't get it to work, try again
" -----------------------------------------------

" -----------------------------------------------
" bootstrap-snippets autocomplete
" bootstrap snippets in ~/.vim/bundles/
" see: https://github.com/bonsaiben/bootstrap-snippets
" -----------------------------------------------

set dictionary+=~/.vim/bundle/bootstrap-snippets/dictionary
set complete+=k

" ----------------------------------------------
" HashiCorp Terraform
" ----------------------------------------------

" Allow vim-terraform to align settings automatically with Tabularize.
let g:terraform_align=1

" Allow vim-terraform to automatically fold (hide until unfolded) sections of terraform code. Defaults to 0 which is off.
let g:terraform_fold_sections=1

" Allow vim-terraform to automatically format *.tf and *.tfvars files with terraform fmt. You can also do this manually with the :TerraformFmt command.
let g:terraform_fmt_on_save=1
