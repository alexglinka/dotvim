git clone https://alexglinka@bitbucket.org/alexglinka/dotvim.git ~/.vim # clone vim settings from repo
cd ~/.vim/
./submodules.sh # initialize and update vim submodules
